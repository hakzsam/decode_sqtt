#include <sys/stat.h>
#include <sys/mman.h>
#include <errno.h>
#include <string.h>
#include <stdarg.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <assert.h>
#include <inttypes.h>

#include "sqtt_file_format.h"

static void
sqtt_print_file_header(SqttFileHeader *header)
{
    fprintf(stderr, "===== SqttFileHeader =====\n");
    fprintf(stderr, "magicNumber: 0x%x\n", header->magicNumber);
    fprintf(stderr, "versionMajor: %d\n", header->versionMajor);
    fprintf(stderr, "versionMinor: %d\n", header->versionMinor);
    fprintf(stderr, "value: %x\n", header->flags.value);
    fprintf(stderr, "isSemaphoreQueueTimingETW: %d\n", header->flags.isSemaphoreQueueTimingETW);
    fprintf(stderr, "noQueueSemaphoreTimeStamps: %d\n", header->flags.noQueueSemaphoreTimeStamps);
    fprintf(stderr, "chunkOffset: %d\n", header->chunkOffset);
    fprintf(stderr, "second: %d\n", header->second);
    fprintf(stderr, "minute: %d\n", header->minute);
    fprintf(stderr, "hour: %d\n", header->hour);
    fprintf(stderr, "dayInMonth: %d\n", header->dayInMonth);
    fprintf(stderr, "month: %d\n", header->month);
    fprintf(stderr, "year: %d\n", header->year);
    fprintf(stderr, "dayInWeek: %d\n", header->dayInWeek);
    fprintf(stderr, "dayInYear: %d\n", header->dayInYear);
    fprintf(stderr, "isDaylightSavings: %d\n", header->isDaylightSavings);
}

static void
sqtt_print_cpu_info(SqttFileChunkCpuInfo *info)
{
    fprintf(stderr, "===== SqttFileChunkCpuInfo =====\n");
    fprintf(stderr, "cpuTimestampFrequency: %ld\n", info->cpuTimestampFrequency);
    fprintf(stderr, "vendorId: %s\n", (char *)info->vendorId);
    fprintf(stderr, "processorBrand: %s\n", (char *)info->processorBrand);
    fprintf(stderr, "clockSpeed: %d\n", info->clockSpeed);
    fprintf(stderr, "numLogicalCores: %d\n", info->numLogicalCores);
    fprintf(stderr, "numPhysicalCores: %d\n", info->numPhysicalCores);
    fprintf(stderr, "systemRamSize: %d\n", info->systemRamSize);
}

static const char *
sqtt_get_gpu_type(SqttGpuType type)
{
    switch (type) {
    case SQTT_GPU_TYPE_UNKNOWN:
        return "UNKNOWN";
    case SQTT_GPU_TYPE_INTEGRATED:
        return "INTEGRATED";
    case SQTT_GPU_TYPE_DISCRETE:
        return "DISCRETE";
    case SQTT_GPU_TYPE_VIRTUAL:
        return "VIRTUAL";
    default:
        assert(0);
    }
}

static const char *
sqtt_get_gfx_ip_level(SqttGfxIpLevel level)
{
    switch (level) {
    case SQTT_GFXIP_LEVEL_NONE:
        return "NONE";
    case SQTT_GFXIP_LEVEL_GFXIP_6:
        return "GFXIP_6";
    case SQTT_GFXIP_LEVEL_GFXIP_7:
        return "GFXIP_7";
    case SQTT_GFXIP_LEVEL_GFXIP_8:
        return "GFXIP_8";
    case SQTT_GFXIP_LEVEL_GFXIP_8_1:
        return "GFXIP_8_1";
    case SQTT_GFXIP_LEVEL_GFXIP_9:
        return "GFXIP_9";
    case SQTT_GFXIP_LEVEL_GFXIP_10_1:
        return "GFXIP_10_1";
    case SQTT_GFXIP_LEVEL_GFXIP_10_3:
        return "GFXIP_10_3";

    default:
        assert(0);
    }
}

static const char *
sqtt_get_memory_type(SqttMemoryType type)
{
    switch (type) {
    case SQTT_MEMORY_TYPE_UNKNOWN:
        return "UNKNOWN";
    case SQTT_MEMORY_TYPE_DDR:
        return "DDR";
    case SQTT_MEMORY_TYPE_DDR2:
        return "DDR2";
    case SQTT_MEMORY_TYPE_DDR3:
        return "DDR3";
    case SQTT_MEMORY_TYPE_DDR4:
        return "DDR4";
    case SQTT_MEMORY_TYPE_GDDR3:
        return "GDDR3";
    case SQTT_MEMORY_TYPE_GDDR4:
        return "GDDR4";
    case SQTT_MEMORY_TYPE_GDDR5:
        return "GDDD5";
    case SQTT_MEMORY_TYPE_GDDR6:
        return "GDDR6";
    case SQTT_MEMORY_TYPE_HBM:
        return "HBM";
    case SQTT_MEMORY_TYPE_HBM2:
        return "HBM2";
    case SQTT_MEMORY_TYPE_HBM3:
        return "HBM3";
    default:
        assert(0);
    }
}

static void
sqtt_print_asic_info(SqttFileChunkAsicInfo *info)
{
    fprintf(stderr, "===== SqttFileChunkAsicInfo =====\n");
    fprintf(stderr, "flags: ");
    if (info->flags & SQTT_FILE_CHUNK_ASIC_INFO_FLAG_SC_PACKER_NUMBERING)
        fprintf(stderr, "SC_PACKER_NUMBERING |");
    if (info->flags & SQTT_FILE_CHUNK_ASIC_INFO_FLAG_PS1_EVENT_TOKENS_ENABLED)
        fprintf(stderr, "PS1_EVENT_TOKENS_ENABLED |");
    fprintf(stderr, "\n");
    fprintf(stderr, "traceShaderCoreClock: %lu\n", info->traceShaderCoreClock);
    fprintf(stderr, "traceMemoryClock: %lu\n", info->traceMemoryClock);
    fprintf(stderr, "deviceId: %x\n", info->deviceId);
    fprintf(stderr, "deviceRevisionId: %x\n", info->deviceRevisionId);
    fprintf(stderr, "vgprsPerSimd: %d\n", info->vgprsPerSimd);
    fprintf(stderr, "sgprsPerSimd: %d\n", info->sgprsPerSimd);
    fprintf(stderr, "shaderEngines: %d\n", info->shaderEngines);
    fprintf(stderr, "computeUnitPerShaderEngine: %d\n", info->computeUnitPerShaderEngine);
    fprintf(stderr, "simdPerComputeUnit: %d\n", info->simdPerComputeUnit);
    fprintf(stderr, "wavefrontsPerSimd: %d\n", info->wavefrontsPerSimd);
    fprintf(stderr, "minimumVgprAlloc: %d\n", info->minimumVgprAlloc);
    fprintf(stderr, "vgprAllocGranularity: %d\n", info->vgprAllocGranularity);
    fprintf(stderr, "minimumSgprAlloc: %d\n", info->minimumSgprAlloc);
    fprintf(stderr, "sgprAllocGranularity: %d\n", info->sgprAllocGranularity);
    fprintf(stderr, "hardwareContexts: %d\n", info->hardwareContexts);
    fprintf(stderr, "gpuType: %s\n", sqtt_get_gpu_type(info->gpuType));
    fprintf(stderr, "gfxIpLevel: %s\n", sqtt_get_gfx_ip_level(info->gfxIpLevel));
    fprintf(stderr, "gpuIndex: %d\n", info->gpuIndex);
    fprintf(stderr, "gdsSize: %d\n", info->gdsSize);
    fprintf(stderr, "gdsPerShaderEngine: %d\n", info->gdsPerShaderEngine);
    fprintf(stderr, "ceRamSize: %d\n", info->ceRamSize);
    fprintf(stderr, "ceRamSizeGraphics: %d\n", info->ceRamSizeGraphics);
    fprintf(stderr, "ceRamSizeCompute: %d\n", info->ceRamSizeCompute);
    fprintf(stderr, "maxNumberOfDedicatedCus: %d\n", info->maxNumberOfDedicatedCus);
    fprintf(stderr, "vramSize: %ld\n", info->vramSize);
    fprintf(stderr, "vramBusWidth: %d\n", info->vramBusWidth);
    fprintf(stderr, "l2CacheSize: %d\n", info->l2CacheSize);
    fprintf(stderr, "l1CacheSize: %d\n", info->l1CacheSize);
    fprintf(stderr, "ldsSize: %d\n", info->ldsSize);
    fprintf(stderr, "gpuName: %s\n", info->gpuName);
    fprintf(stderr, "aluPerClock: %f\n", info->aluPerClock);
    fprintf(stderr, "texturePerClock: %f\n", info->texturePerClock);
    fprintf(stderr, "primsPerClock: %f\n", info->primsPerClock);
    fprintf(stderr, "pixelsPerClock: %f\n", info->pixelsPerClock);
    fprintf(stderr, "gpuTimestampFrequency: %lu\n", info->gpuTimestampFrequency);
    fprintf(stderr, "maxShaderCoreClock: %lu\n", info->maxShaderCoreClock);
    fprintf(stderr, "maxMemoryClock: %lu\n", info->maxMemoryClock);
    fprintf(stderr, "memoryOpsPerClock: %d\n", info->memoryOpsPerClock);
    fprintf(stderr, "memoryChipType: %s\n", sqtt_get_memory_type(info->memoryChipType));
    fprintf(stderr, "ldsGranularity: %d\n", info->ldsGranularity);
    for (unsigned se = 0; se < 32; se++) {
        for (unsigned sa = 0; sa < 2; sa++) {
            if (!info->cuMask[se][sa])
                continue;
            fprintf(stderr, "cuMask[%d][%d]=%x\n", se, sa, info->cuMask[se][sa]);
        }
    }
    fprintf(stderr, "reserverd1: %s\n", info->reserved1);
}

static const char *
sqtt_get_api_type(SqttApiType type)
{
    switch (type) {
    case SQTT_API_TYPE_DIRECTX_12:
        return "DX12";
    case SQTT_API_TYPE_VULKAN:
        return "VULKAN";
    case SQTT_API_TYPE_GENERIC:
        return "GENERIC";
    case SQTT_API_TYPE_OPENCL:
        return "OPENCL";
    default:
        assert(0);
    }
}

static const char *
sqtt_get_profiling_mode(SqttProfilingMode mode)
{
    switch (mode) {
    case SQTT_PROFILING_MODE_PRESENT:
        return "PRESENT";
    case SQTT_PROFILING_MODE_USER_MARKERS:
        return "USER_MARKERS";
    case SQTT_PROFILING_MODE_INDEX:
        return "INDEX";
    case SQTT_PROFILING_MODE_TAG:
        return "TAG";
    default:
        assert(0);
    }
}

static const char *
sqtt_get_instruction_trace_mode(SqttInstructionTraceMode mode)
{
    switch (mode) {
    case SQTT_INSTRUCTION_TRACE_DISABLED:
        return "DISABLED";
    case SQTT_INSTRUCTION_TRACE_FULL_FRAME:
        return "FULL_FRAME";
    case SQTT_INSTRUCTION_TRACE_API_PSO:
        return "API_PSO";
    default:
        assert(0);
    }
}

static void
sqtt_print_api_info(SqttFileChunkApiInfo *info)
{
    fprintf(stderr, "===== SqttFileChunkApiInfo =====\n");
    fprintf(stderr, "apiType: %s\n", sqtt_get_api_type(info->apiType));
    fprintf(stderr, "versionMajor: %d\n", info->versionMajor);
    fprintf(stderr, "versionMinor: %d\n", info->versionMinor);
    fprintf(stderr, "profilingMode: %s\n", sqtt_get_profiling_mode(info->profilingMode));
    fprintf(stderr, "reserved: %d\n", info->reserved);
    fprintf(stderr, "profilingModeData.userMarkerProfilingData.start: %s\n",
            info->profilingModeData.userMarkerProfilingData.start);
    fprintf(stderr, "profilingModeData.userMarkerProfilingData.end: %s\n",
            info->profilingModeData.userMarkerProfilingData.end);
    fprintf(stderr, "profilingModeData.indexProfilingData.start: %d\n",
            info->profilingModeData.indexProfilingData.start);
    fprintf(stderr, "profilingModeData.indexProfilingData.end: %d\n",
            info->profilingModeData.indexProfilingData.end);
    fprintf(stderr, "profilingModedata.tagProfilingData.beginHi: %d\n",
            info->profilingModeData.tagProfilingData.beginHi);
    fprintf(stderr, "profilingModedata.tagProfilingData.beginLo: %d\n",
            info->profilingModeData.tagProfilingData.beginLo);
    fprintf(stderr, "profilingModedata.tagProfilingData.endHi: %d\n",
            info->profilingModeData.tagProfilingData.endHi);
    fprintf(stderr, "profilingModedata.tagProfilingData.endLo: %d\n",
            info->profilingModeData.tagProfilingData.endLo);
    fprintf(stderr, "instructionTraceMode: %s\n", sqtt_get_instruction_trace_mode(info->instructionTraceMode));
    fprintf(stderr, "reserved2: %d\n", info->reserved2);
    fprintf(stderr, "instructionTraceData.apiPsoFilter: %" PRIx64 "\n",
            info->instructionTraceData.apiPsoData.apiPsoFilter);
    fprintf(stderr, "instructionTraceData.userMarkerData.start: %s\n",
            info->instructionTraceData.userMarkerData.start);
    fprintf(stderr, "instructionTraceData.userMarkerData.end: %s\n",
            info->instructionTraceData.userMarkerData.end);
}

static const char *
sqtt_get_version(SqttVersion version)
{
    switch (version) {
    case SQTT_VERSION_NONE:
        return "NONE";
    case SQTT_VERSION_1_0:
        return "1.0";
    case SQTT_VERSION_1_1:
        return "1.1";
    case SQTT_VERSION_2_0:
        return "2.0";
    case SQTT_VERSION_2_1:
        return "2.1";
    case SQTT_VERSION_2_2:
        return "2.2";
    case SQTT_VERSION_2_3:
        return "2.3";
    case SQTT_VERSION_2_4:
        return "2.4";
    default:
        assert(0);
    }
}

static void
sqtt_print_sqtt_desc(SqttFileChunkSqttDesc *desc)
{
    fprintf(stderr, "===== SqttFileChunkSqttDesc =====\n");
    fprintf(stderr, "shaderEngineIndex: %d\n", desc->shaderEngineIndex);
    fprintf(stderr, "v1.instrumentationSpecVersion: %d\n", desc->v1.instrumentationSpecVersion);
    fprintf(stderr, "v1.instrumentationApiVersion: %d\n", desc->v1.instrumentationApiVersion);
    fprintf(stderr, "v1.computeUnitIndex: %d\n", desc->v1.computeUnitIndex);
    fprintf(stderr, "sqttVersion: %s\n", sqtt_get_version(desc->sqttVersion));
}

static void
sqtt_print_sqtt_data(SqttFileChunkSqttData *data)
{
    fprintf(stderr, "===== SqttFileChunkSqttData =====\n");
    fprintf(stderr, "offset: %d\n", data->offset);
    fprintf(stderr, "size: %d\n", data->size);
}

static void
sqtt_print_code_object_database(SqttFileChunkCodeObjectDatabase *chunk)
{
    fprintf(stderr, "===== SqttFileChunkCodeObjectDatabase =====\n");
    fprintf(stderr, "offset: %u\n", chunk->offset);
    fprintf(stderr, "flags: %u\n", chunk->flags);
    fprintf(stderr, "size: %u\n", chunk->size);
    fprintf(stderr, "recordCount: %u\n", chunk->recordCount);
}

static void
sqtt_print_code_object_database_record(SqttCodeObjectDatabaseRecord *record)
{
    fprintf(stderr, "\t===== SqttCodeObjectDatabaseRecord =====\n");
    fprintf(stderr, "\trecordSize: %d\n", record->recordSize);
}

static void
sqtt_print_code_object_loader_events(SqttFileChunkCodeObjectLoaderEvents *chunk)
{
    fprintf(stderr, "===== SqttFileChunkCodeObjectLoaderEvents =====\n");
    fprintf(stderr, "offset: %u\n", chunk->offset);
    fprintf(stderr, "flags: %u\n", chunk->flags);
    fprintf(stderr, "recordSize: %u\n", chunk->recordSize);
    fprintf(stderr, "recordCount: %u\n", chunk->recordCount);
}

static void
sqtt_print_pso_correlation(SqttFileChunkPsoCorrelation *chunk)
{
    fprintf(stderr, "===== SqttFileChunkPsoCorrelation =====\n");
    fprintf(stderr, "offset: %u\n", chunk->offset);
    fprintf(stderr, "flags: %u\n", chunk->flags);
    fprintf(stderr, "recordSize: %u\n", chunk->recordSize);
    fprintf(stderr, "recordCount: %u\n", chunk->recordCount);
}

static void
sqtt_print_pso_correlation_record(SqttPsoCorrelationRecord *record)
{
    fprintf(stderr, "\t===== SqttPsoCorrelationRecord =====\n");
    fprintf(stderr, "\tapiPsoHash: 0x%" PRIx64 "\n", record->apiPsoHash);
    fprintf(stderr, "\tinternalPipelineHash.lower: 0x%" PRIx64 "\n",
            record->internalPipelineHash.lower);
    fprintf(stderr, "\tinternalPipelineHash.upper: 0x%" PRIx64 "\n",
            record->internalPipelineHash.upper);
    fprintf(stderr, "\tapiObjectName: %s\n", record->apiObjectName);
}

static void
sqtt_print_isa_database(SqttFileChunkIsaDatabase *chunk)
{
    fprintf(stderr, "===== SqttFileChunkIsaDatabase =====\n");
    fprintf(stderr, "offset: %u\n", chunk->offset);
    fprintf(stderr, "size: %u\n", chunk->size);
    fprintf(stderr, "recordCount: %u\n", chunk->recordCount);
}

static void
sqtt_print_isa_db_record(SqttIsaDbRecord *record)
{
    fprintf(stderr, "\t===== SqttIsaDbRecord =====\n");
    fprintf(stderr, "\tshaderStage: %x\n", record->shaderStage);
    fprintf(stderr, "\trecordSize: %d\n", record->recordSize);
}

static void
sqtt_print_shader_isa_blob_header(SqttShaderIsaBlobHeader *blob)
{
    fprintf(stderr, "\t ===== SqttShaderIsaBlobHeader =====\n");
    fprintf(stderr, "\tsizeInBytes: %d\n", blob->sizeInBytes);
    fprintf(stderr, "\tactualVgprCount: %d\n", blob->actualVgprCount);
    fprintf(stderr, "\tactualSgprCount: %d\n", blob->actualSgprCount);
    fprintf(stderr, "\tapiShaderHash.lower: 0x%" PRIx64 "\n", blob->apiShaderHash.lower);
    fprintf(stderr, "\tapiShaderHash.upper: 0x%" PRIx64 "\n", blob->apiShaderHash.upper);
    fprintf(stderr, "\tpalShaderHash.lower: 0x%" PRIx64 "\n", blob->palShaderHash.lower);
    fprintf(stderr, "\tpalShaderHash.upper: 0x%" PRIx64 "\n", blob->palShaderHash.upper);
    fprintf(stderr, "\tactualLdsCount: %d\n", blob->actualLdsCount);
    fprintf(stderr, "\tbaseAddress: %" PRIx64 "\n", blob->baseAddress);
    fprintf(stderr, "\tscratchSize: %d\n", blob->scratchSize);
    fprintf(stderr, "\tflags: %x\n", blob->flags);

}

static const char *
sqtt_get_code_object_loader_event_type(SqttCodeObjectLoaderEventType type)
{
    switch (type) {
    case SQTT_CODE_OBJECT_LOAD_TO_GPU_MEMORY:
        return "LOAD_TO_GPU_MEMORY";
    case SQTT_CODE_OBJECT_UNLOAD_FROM_GPU_MEMORY:
        return "UNLOAD_FROM_GPU_MEMORY";
    default:
        assert(0);
    }
}

static void
sqtt_print_code_object_loader_event_record(SqttCodeObjectLoaderEventRecord *record)
{
    fprintf(stderr, "\t===== SqttCodeObjectLoaderEventRecord =====\n");
    fprintf(stderr, "\teventType: %s\n", sqtt_get_code_object_loader_event_type(record->eventType));
    fprintf(stderr, "\tbaseAddress: 0x%" PRIx64 "\n", record->baseAddress);
    fprintf(stderr, "\tcodeObjectHash.lower: 0x%" PRIx64 " (%" PRId64 ")\n",
            record->codeObjectHash.lower,
            record->codeObjectHash.lower);
    fprintf(stderr, "\tcodeObjectHash.upper: 0x%" PRIx64 " (%" PRId64 ")\n",
            record->codeObjectHash.upper,
            record->codeObjectHash.upper);
    fprintf(stderr, "\ttimestamp: %" PRId64 "\n", record->timestamp);
}

static void
sqtt_print_queue_event_timings(SqttFileChunkQueueEventTimings *chunk)
{
    fprintf(stderr, "===== SqttFileChunkQueueEventTimings =====\n");
    fprintf(stderr, "chunkIndex %d\n", chunk->header.chunkIdentifier.chunkIndex);
    fprintf(stderr, "queueInfoTableRecordCount: %d\n", chunk->queueInfoTableRecordCount);
    fprintf(stderr, "queueInfoTableSize: %d\n", chunk->queueInfoTableSize);
    fprintf(stderr, "queueEventTableRecordCount: %d\n", chunk->queueEventTableRecordCount);
    fprintf(stderr, "queueEventTableSize: %d\n", chunk->queueEventTableSize);

}

static void
sqtt_print_queue_info_record(SqttQueueInfoRecord *record)
{
    fprintf(stderr, "===== SqttQueueInfoRecord =====\n");
    fprintf(stderr, "queueID: %" PRId64 "\n", record->queueID);
    fprintf(stderr, "queueContext: %" PRId64 "\n", record->queueContext);
    fprintf(stderr, "hardwareInfo.queueType: %d\n", record->hardwareInfo.queueType);
    fprintf(stderr, "hardwareInfo.engineType: %d\n", record->hardwareInfo.engineType);
}

static const char *event_type_str(uint32_t event_type)
{
    switch (event_type) {
    case SQTT_QUEUE_TIMING_EVENT_CMDBUF_SUBMIT:
        return "cmdbuf_submit";
    case SQTT_QUEUE_TIMING_EVENT_SIGNAL_SEMAPHORE:
        return "signal_semaphore";
    case SQTT_QUEUE_TIMING_EVENT_WAIT_SEMAPHORE:
        return "wait_semaphore";
    case SQTT_QUEUE_TIMING_EVENT_PRESENT:
        return "present";
    default:
        return "unknown event";
    }
}

static void
sqtt_print_queue_event_record(SqttQueueEventRecord *record)
{
    fprintf(stderr, "===== SqttQueueEventRecord =====\n");
    fprintf(stderr, "eventType: %s\n", event_type_str(record->eventType));
    fprintf(stderr, "sqttCbId: %d\n", record->sqttCbId);
    fprintf(stderr, "frameIndex: %"PRId64"\n", record->frameIndex);
    fprintf(stderr, "queueInfoIndex: %d\n", record->queueInfoIndex);
    fprintf(stderr, "submitSubIndex: %d\n", record->submitSubIndex);
    fprintf(stderr, "apiId: 0x%"PRIx64"\n", record->apiId);
    fprintf(stderr, "cpuTimestamp: %"PRId64"\n", record->cpuTimestamp);
    fprintf(stderr, "gpuTimestamp[0]: %"PRId64"\n", record->gpuTimestamps[0]);
    fprintf(stderr, "gpuTimestamp[1]: %"PRId64"\n", record->gpuTimestamps[1]);
}

static void
sqtt_print_clock_calibration(SqttFileChunkClockCalibration *chunk)
{
    fprintf(stderr, "===== SqttFileChunkClockCalibration =====\n");
    fprintf(stderr, "chunkIndex %d\n", chunk->header.chunkIdentifier.chunkIndex);
    fprintf(stderr, "cpuTimestamps: %" PRId64 "\n", chunk->cpuTimestamp);
    fprintf(stderr, "gpuTimestamps: %" PRId64 "\n", chunk->gpuTimestamp);
}

static void
sqtt_print_spm_db(SqttFileChunkSpmDb *chunk)
{
    fprintf(stderr, "===== SqttFileChunkSpmDb =====\n");
    fprintf(stderr, "flags: %x\n", chunk->flags);
    fprintf(stderr, "numTimestamps: %d\n", chunk->numTimestamps);
    fprintf(stderr, "numSpmCounterInfo: %d\n", chunk->numSpmCounterInfo);
    fprintf(stderr, "samplingInterval: %d\n", chunk->samplingInterval);
}

int main(int argc, char **argv)
{
    size_t size, file_offset = 0;
    const char *mapped;
    char *input_file;
    struct stat s;
    int fd, ret;

    if (argc != 2) {
        fprintf(stderr, "./decode_sqtt <*.rgp>\n");
        return 1;
    }

    input_file = argv[1];
    fprintf(stderr, "Decoding '%s'...\n", input_file);

    fd = open(input_file, O_RDONLY);
    if (fd < 0) {
        fprintf(stderr, "open() failed\n");
        return 1;
    }

    ret = fstat(fd, &s);
    if (ret < 0) {
        close(fd);
        fprintf(stderr, "stat() failed\n");
        return 1;
    }
    size = s.st_size;

    fprintf(stderr, "size: %ld\n", size);

    mapped = (char*)mmap(0, size, PROT_READ, MAP_PRIVATE, fd, 0);
    if (mapped == MAP_FAILED) {
        close(fd);
        fprintf(stderr, "mmap() failed\n");
        return 1;
    }

    SqttFileHeader *header = (SqttFileHeader *)mapped;
    sqtt_print_file_header(header);
    file_offset += sizeof(*header);
    mapped += file_offset;

    while (file_offset < size) {
        SqttFileChunkHeader *header = (SqttFileChunkHeader *)mapped;
        SqttFileChunkType chunk_type = header->chunkIdentifier.chunkType;
        int32_t chunk_size = header->sizeInBytes;

        switch (chunk_type) {
        case SQTT_FILE_CHUNK_TYPE_CPU_INFO: {
            SqttFileChunkCpuInfo *chunk = (SqttFileChunkCpuInfo *)mapped;
            sqtt_print_cpu_info(chunk);
            break;
        }
        case SQTT_FILE_CHUNK_TYPE_ASIC_INFO: {
            SqttFileChunkAsicInfo *chunk = (SqttFileChunkAsicInfo *)mapped;
            sqtt_print_asic_info(chunk);
            break;
        }
        case SQTT_FILE_CHUNK_TYPE_API_INFO: {
            SqttFileChunkApiInfo *chunk = (SqttFileChunkApiInfo *)mapped;
            sqtt_print_api_info(chunk);
            break;
        }
        case SQTT_FILE_CHUNK_TYPE_SQTT_DESC: {
            SqttFileChunkSqttDesc *chunk = (SqttFileChunkSqttDesc *)mapped;
            sqtt_print_sqtt_desc(chunk);
            break;
        }
        case SQTT_FILE_CHUNK_TYPE_SQTT_DATA: {
            SqttFileChunkSqttData *data = (SqttFileChunkSqttData *)mapped;
            sqtt_print_sqtt_data(data);

            uint32_t *ptr = (uint32_t *)(mapped + sizeof(*data));
            for (unsigned i = 0; i < 4; i++) {
                fprintf(stderr, "ptr[%d]=%x\n", i, ptr[i]);
            }
            break;
        }
        case SQTT_FILE_CHUNK_TYPE_CODE_OBJECT_DATABASE: {
            SqttFileChunkCodeObjectDatabase *chunk = (SqttFileChunkCodeObjectDatabase *)mapped;
            sqtt_print_code_object_database(chunk);
            //assert(chunk->recordCount == 1);
            uint8_t *ptr = (uint8_t *)(mapped + sizeof(*chunk));
            for (unsigned i = 0; i < chunk->recordCount; i++) {
                SqttCodeObjectDatabaseRecord *record =
                    (SqttCodeObjectDatabaseRecord *)(ptr);//mapped + sizeof(*chunk) + sizeof(*record) * i);
                sqtt_print_code_object_database_record(record);

                uint8_t *code_ptr = (uint8_t *)(ptr + sizeof(*record));
                char filename[128];
                sprintf(filename, "/tmp/code_object_%d", i);
                FILE *f = fopen(filename, "w+");
                fwrite(code_ptr, record->recordSize, 1, f);
                fclose(f);

                ptr += sizeof(*record) + record->recordSize;
            }
            break;
        }
        case SQTT_FILE_CHUNK_TYPE_CODE_OBJECT_LOADER_EVENTS: {
            SqttFileChunkCodeObjectLoaderEvents *chunk = (SqttFileChunkCodeObjectLoaderEvents *)mapped;
            sqtt_print_code_object_loader_events(chunk);

            for (unsigned i = 0; i < chunk->recordCount; i++) {
                SqttCodeObjectLoaderEventRecord *record =
                    (SqttCodeObjectLoaderEventRecord *)(mapped + sizeof(*chunk) + sizeof(*record) * i);
                sqtt_print_code_object_loader_event_record(record);
            }
            break;
        }
        case SQTT_FILE_CHUNK_TYPE_PSO_CORRELATION: {
            SqttFileChunkPsoCorrelation *chunk = (SqttFileChunkPsoCorrelation *)mapped;
            sqtt_print_pso_correlation(chunk);

            for (unsigned i = 0; i < chunk->recordCount; i++) {
                SqttPsoCorrelationRecord *record =
                    (SqttPsoCorrelationRecord *)(mapped + sizeof(*chunk) + sizeof(*record) * i);
                sqtt_print_pso_correlation_record(record);
            }
            break;
        }
        case SQTT_FILE_CHUNK_TYPE_ISA_DATABASE: {
            SqttFileChunkIsaDatabase *chunk = (SqttFileChunkIsaDatabase *)mapped;
            sqtt_print_isa_database(chunk);

            const char *tmp_ptr = mapped + sizeof(*chunk);
            for (unsigned i = 0; i < chunk->recordCount; i++) {
                SqttIsaDbRecord *record = (SqttIsaDbRecord *)tmp_ptr;
                SqttShaderIsaBlobHeader *blob =
                    (SqttShaderIsaBlobHeader *)(tmp_ptr + sizeof(*record));
                sqtt_print_isa_db_record(record);
                sqtt_print_shader_isa_blob_header(blob);
                tmp_ptr += record->recordSize;
            }
            break;
        }
        case SQTT_FILE_CHUNK_TYPE_QUEUE_EVENT_TIMINGS: {
            SqttFileChunkQueueEventTimings *chunk = (SqttFileChunkQueueEventTimings *)mapped;
            sqtt_print_queue_event_timings(chunk);

            const char *info_ptr = mapped + sizeof(*chunk);
            for (unsigned i = 0; i < chunk->queueInfoTableRecordCount; i++) {
                SqttQueueInfoRecord *record = (SqttQueueInfoRecord *)info_ptr;
                sqtt_print_queue_info_record(record);
                info_ptr += sizeof(*record);
            }

            for (unsigned i = 0; i < chunk->queueEventTableRecordCount; i++) {
                SqttQueueEventRecord *record = (SqttQueueEventRecord *)info_ptr;
                sqtt_print_queue_event_record(record);
                info_ptr += sizeof(*record);
            }

            break;
        }
        case SQTT_FILE_CHUNK_TYPE_CLOCK_CALIBRATION: {
            SqttFileChunkClockCalibration *chunk = (SqttFileChunkClockCalibration *)mapped;
            sqtt_print_clock_calibration(chunk);
            break;
        }
        case SQTT_FILE_CHUNK_TYPE_SPM_DB: {
            SqttFileChunkSpmDb *chunk = (SqttFileChunkSpmDb *)mapped;
            sqtt_print_spm_db(chunk);

            uint8_t *data_ptr = (uint8_t *)(mapped + sizeof(*chunk));

            fprintf(stderr, "-- timestamps --\n");
            uint64_t *timestamps = (uint64_t *)data_ptr;
            for (unsigned i = 0; i < chunk->numTimestamps; i++) {
                fprintf(stderr, "timestamp: %"PRId64"\n", timestamps[i]);
            }
            data_ptr += (chunk->numTimestamps * sizeof(uint64_t));

            fprintf(stderr, "-- counters info --\n");
            SpmCounterInfo *counters = (SpmCounterInfo *)data_ptr;
            for (uint32_t i = 0; i < chunk->numSpmCounterInfo; i++) {
                SpmCounterInfo *counter = &counters[i];

                fprintf(stderr, "Counter #%d:\n", i);
                fprintf(stderr, "block: %d\n", counter->block);
                fprintf(stderr, "instance: %d\n", counter->instance);
                fprintf(stderr, "dataOffset: %d\n", counter->dataOffset);
                fprintf(stderr, "eventIndex: %d (0x%x)\n", counter->eventIndex, counter->eventIndex);
                fprintf(stderr, "\n");
            }
            data_ptr += (chunk->numSpmCounterInfo * sizeof(SpmCounterInfo));

            uint16_t *values = (uint16_t *)data_ptr;
            for (uint32_t i = 0; i < chunk->numSpmCounterInfo; i++) {
                for (uint32_t j = 0; j < chunk->numTimestamps; j++) {
                    uint32_t offset = i * chunk->numTimestamps + j;
                    uint16_t value = values[offset];

                    //fprintf(stderr, "value=%d\n", value);
                }
            }
            break;
        }
        default:
            fprintf(stderr, "Unsupported SQTT chunk type: %d\n", chunk_type);
            assert(0);
        }

        file_offset += chunk_size;
        mapped += chunk_size;

        fprintf(stderr, "file_offset=%ld, size=%ld\n", file_offset, size);
    }

    close(fd);
    return 0;
}
